(ns ancient-clj.io.xml-test
  (:require [clojure.test :refer [deftest is testing]]
            [ancient-clj.io.xml :refer [metadata-xml->versions
                                        metadata-path
                                        metadata-uri]]
            [clojure.string :as string]))

;; ## Fixtures

(def snapshot-versions
  ["0.1.0-SNAPSHOT" "0.1.1-SNAPSHOT" "0.1.3-SNAPSHOT"])

(def qualified-versions
  ["0.1.1-RC0" "0.1.3-alpha"])

(def release-versions
  ["0.1.0" "0.1.1" "0.1.2"])

(def versions
  (concat
   snapshot-versions
   qualified-versions
   release-versions))

(defn generate-xml
  []
  (format
   "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
     <metadata>
     <groupId>group</groupId>
     <artifactId>group</artifactId>
     <versioning>
     <release>%s</release>
     <versions>%s</versions>
     <lastUpdated>20140930173943</lastUpdated>
     </versioning>
     </metadata>"
   (last release-versions)
   (->> (for [v (shuffle versions)]
        (format "<version>%s</version>" v))
      (string/join "\n"))))

;; ## Tests
(deftest xml-version-collection
  (let [vs (-> (generate-xml)
              (metadata-xml->versions)
              (set))]
    (is (every? string? vs))
    (is (= (count vs) (count versions)))
    (is (every? vs snapshot-versions))
    (is (every? vs qualified-versions))
    (is (every? vs release-versions))))

(deftest path-creation
  (let [uri      ["http://nexus.me/"]
        pandect0 ["pandect" "pandect" nil "pandect/pandect/maven-metadata.xml"]
        pandect1 ["pandect" "pandect" "xy.xml"]
        clojure0 ["org.clojure" "clojure" nil]
        clojure1 ["org.clojure" "data.json" nil]
        clojure2 ["org.clojure" "data.json" "xy.xml"]
        some0    ["some" "my.pkg" "xy.xml"]]
    (testing "pandect0"
      (let [t     (apply metadata-path pandect0)
            uri-t (apply metadata-uri (concat uri pandect0))]
        (is (= "pandect/pandect/maven-metadata.xml"
               t))
        (is (= (str (first uri) t)
               uri-t))))
    (testing "pandect1"
      (let [t     (apply metadata-path pandect1)
            uri-t (apply metadata-uri (concat uri pandect1))]
        (is (= "pandect/pandect/xy.xml"
               t))
        (is (= (str (first uri) t)
               uri-t))))
    (testing "pandect1"
      (let [t     (apply metadata-path pandect1)
            uri-t (apply metadata-uri (concat uri pandect1))]
        (is (= "pandect/pandect/xy.xml"
               t))
        (is (= (str (first uri) t)
               uri-t))))
    (testing "clojure0"
      (let [t     (apply metadata-path clojure0)
            uri-t (apply metadata-uri (concat uri clojure0))]
        (is (= "org/clojure/clojure/maven-metadata.xml"
               t))
        (is (= (str (first uri) t)
               uri-t))))
    (testing "clojure1"
      (let [t     (apply metadata-path clojure1)
            uri-t (apply metadata-uri (concat uri clojure1))]
        (is (= "org/clojure/data.json/maven-metadata.xml" t))
        (is (= (str (first uri) t)
               uri-t))))
    (testing "clojure2"
      (let [t     (apply metadata-path clojure2)
            uri-t (apply metadata-uri (concat uri clojure2))]
        (is (= "org/clojure/data.json/xy.xml" t))
        (is (= (str (first uri) t)
               uri-t))))
    (testing "some0"
      (let [t     (apply metadata-path some0)
            uri-t (apply metadata-uri (concat uri some0))]
        (is (= "some/my.pkg/xy.xml" t))
        (is (= (str (first uri) t)
               uri-t))))))

;; (require '[clojure.test :refer [run-tests]])
;; (run-tests 'ancient-clj.io.xml-test)
