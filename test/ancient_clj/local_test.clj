(ns ancient-clj.io.local-test
  (:require [clojure.test :as t :refer [deftest use-fixtures is testing]]
            [clojure.java.io :as io]
            [ancient-clj.io.local :refer [local-loader]]
            [ancient-clj.io.xml :refer [metadata-path]]
            [ancient-clj.io.xml-test :as xml])
  (:import [java.io File]))

(def paths-atom (atom []))

(defn before-test [paths]
  (let [dir (doto (File/createTempFile "ancient" "repo")
              (.delete))
        f   (io/file dir (metadata-path "group" "id"))
        f'  (->> (metadata-path
                "group" "other"
                "maven-metadata-local.xml")
               (io/file dir))]
    (doto (.getParentFile f)
      (.mkdirs))
    (doto (.getParentFile f')
      (.mkdirs))
    (spit f (xml/generate-xml))
    (spit f' (xml/generate-xml))
    (->> (iterate #(.getParentFile %) f)
       (take 4)
       (cons f')
       (reverse)
       (reset! paths))))

(defn after-test []
  (doseq [f (reverse @paths-atom)]
    (.delete f)))

(defn setup-teardown [f]
  (before-test paths-atom)
  (f)
  (after-test))

(use-fixtures :once setup-teardown)

(deftest local-XMLversion-loader
  #_(tabular
     (fact "about the "
           (let [loader (local-loader (first @paths))
                 vs     (set (loader "group" ?id))]
             vs => (has every? string?)
             (count vs) => (count xml/versions)
             xml/snapshot-versions => (has every? vs)
             xml/qualified-versions => (has every? vs)
             xml/release-versions => (has every? vs)))
     ?id
     "id"
     "other")
  (testing "missing artifact"
    (let [loader (local-loader (first @paths-atom))]
      (is (empty? (loader "group" "invalid"))))))
#_(t/run-tests 'ancient-clj.io.local-test)
